/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uchicago.gerber;

import static edu.uchicago.gerber.WebViewSample.DEFAULT_URL;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * An implementation of tabs using the TabPane class.
 *
 * @see javafx.scene.control.TabPane
 * @see javafx.scene.control.Tab
 * @see javafx.scene.control.TabPane
 */
public class TabSampleDrag extends Application {

    final TabPane tabPane = new TabPane();
    private int nTabCount = 0;

    private void init(Stage primaryStage) {
       // Group root = new Group();
        BorderPane borderPane = new BorderPane();
        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        

        final Button btn = new Button();
        
       
        
        
        btn.setPrefSize(500, 30);
        btn.setText("Click to add tab");

        tabPane.setPrefSize(500, 600);
        tabPane.setSide(Side.TOP);

        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.ALL_TABS);

//        root.setOnDragDropped(new EventHandler<DragEvent>() {
//            @Override
//            public void handle(DragEvent event) {
//                Dragboard db = event.getDragboard();
//                boolean success = false;
//                if (db.hasContent(DataFormat.PLAIN_TEXT)) {
//                    success = true;
//                    String filePath = null;
//                    StringBuilder stb = new StringBuilder();
//                    File file = db.getFiles().get(0);
//                    filePath = file.getAbsolutePath();
//
//                    final Tab tabNew = new Tab();
//                    tabNew.setText("Tab " + ++nTabCount);
//                    tabNew.setContent(getBroswer(filePath));
//
//                    tabPane.getTabs().addAll(tabNew);
//                    
//        
//
////                    for (File file:db.getFiles()) {
////                        filePath = file.getAbsolutePath();
////                        System.out.println(filePath);
////                        stb.append(filePath + "\n");
////                    }
//                }
//                event.setDropCompleted(success);
//                event.consume();
//            }
//        });

        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                final Tab tabNew = new Tab();
                tabNew.setText("Tab " + ++nTabCount);
                tabNew.setContent(getBroswer());

                tabPane.getTabs().addAll(tabNew);

            }

        });

        final Tab tab1 = new Tab();
        tab1.setText("Tab " + ++nTabCount);
        tab1.setContent(getBroswer());

        //this is var-args
        tabPane.getTabs().addAll(tab1);
        borderPane.setBottom(tabPane);
        borderPane.setTop(btn);
        // borderPane.setTop(hBox);
        
         btn.prefWidthProperty().bind(borderPane.widthProperty());
        
       // borderPane.getChildren().addAll(borderPane);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    //this is now our web-browser embebbed in a VBox
    private VBox getBroswer() {
        //this is our webview which is embeedd in a vBox
        WebView webView = new WebView();

        final WebEngine webEngine = webView.getEngine();
        webEngine.load(DEFAULT_URL);

        final TextField locationField = new TextField(DEFAULT_URL);
        webEngine.locationProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                locationField.setText(newValue);
            }
        });
        EventHandler<ActionEvent> goAction = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
              if(locationField.getText().startsWith("http://")){
                webEngine.load(locationField.getText());
            } else {
                  try { 
                      Desktop.getDesktop().open(new File(locationField.getText().substring(6,locationField.getText().length() )));
                  } catch (IOException ex) {
                      Logger.getLogger(TabSampleDrag.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
                
               
            }
        };
        locationField.setOnAction(goAction);

        Button goButton = new Button("Go");
        goButton.setDefaultButton(true);
        goButton.setOnAction(goAction);

        // Layout logic
        HBox hBox = new HBox(5);
        hBox.getChildren().setAll(locationField, goButton);
        HBox.setHgrow(locationField, Priority.ALWAYS);

        VBox vBox = new VBox(5);
        //this is var-args
        vBox.getChildren().setAll(hBox, webView);
        VBox.setVgrow(webView, Priority.ALWAYS);
        return vBox;
    }

    //overloaded to take string
    //file:///c:/WINDOWS/clock.avi
    private VBox getBroswer(String strLocal) {
        //this is our webview which is embeedd in a vBox
        WebView webView = new WebView();

        String strLocalUrl = "file:///" + strLocal;

        final WebEngine webEngine = webView.getEngine();
        webEngine.load(strLocalUrl);

        final TextField locationField = new TextField(strLocalUrl);
        webEngine.locationProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                locationField.setText(newValue);
            }
        });
        EventHandler<ActionEvent> goAction = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  webEngine.load(locationField.getText());
//                if (locationField.getText().startsWith("http://")) {
//                    webEngine.load(locationField.getText());
//                } else if (locationField.getText().startsWith("file:///")) {
//                    webEngine.load(locationField.getText());
//                } else {
//                    webEngine.load("http://" + locationField.getText());
//                }

            }
        };
        locationField.setOnAction(goAction);

        Button goButton = new Button("Go");
        goButton.setDefaultButton(true);
        goButton.setOnAction(goAction);

        // Layout logic
        HBox hBox = new HBox(5);
        hBox.getChildren().setAll(locationField, goButton);
        HBox.setHgrow(locationField, Priority.ALWAYS);

        VBox vBox = new VBox(5);
        vBox.getChildren().setAll(hBox, webView);
        VBox.setVgrow(webView, Priority.ALWAYS);
        return vBox;
    }
}
