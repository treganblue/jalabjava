/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.gerber.nyt;

import edu.uchicago.gerber.yelp.YelpSearchResults;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ag
 */
public class NytSearchResults {
    
    
    public String status;
    public String copyright;
    public Integer num_results;
    
   public List<Result> results;
    public class Result {

        public String display_title;
        public String mpaa_rating;
        public Integer crit_pick;
        public Link link;
        public class Link {
            public String url;
        }
    }
    
    
    
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    
      public ArrayList<String> getSimpleValues() {
        ArrayList<String> simpleValues = new ArrayList<String>();


        for (NytSearchResults.Result res : results) {
            try {
                simpleValues.add(res.display_title + " | " + res.mpaa_rating  + " | " + res.link.url);
            } catch (Exception e) {
                //will continue on its own
              
            }
        }

        return simpleValues;
    }
    
          //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    
      public ArrayList<String> getUrlValues() {
        ArrayList<String> simpleValues = new ArrayList<String>();


        for (NytSearchResults.Result res : results) {
            try {
                simpleValues.add(res.link.url);
            } catch (Exception e) {
                //will continue on its own
              
            }
        }

        return simpleValues;
    }
}
